#include<stdio.h>
#include<pthread.h>
#include<semaphore.h>
int crgroup[1000000],pref[1000000][8],strm[1000000],nucrs[1000000],alloc[1000000][9],stnu[1000000],left[1000000]={0};
sem_t gate;
FILE* writestrm;
void *calc(void * arg)
{
	sem_wait(&gate);
	int i,j,cur=0,id=*((int*)arg);
	for(i=0;i<8;i++)
	{
			if(nucrs[pref[id][i]]<60)
			{
				alloc[id][++cur]=pref[id][i];
			}
			if(cur==4)break;
	}
	if(cur<4)
	{
			left[id]=1;
			printf("%dth student was left\n",id+1);
	}else{
			for(i=1;i<=4;i++)
			{
					fprintf(writestrm,"%dth student was allocated %dth subject\n",id,alloc[id][i]+1);
					nucrs[alloc[id][i]]++;
			}
	}
	sem_post(&gate);
	return;
}
int main(int argv,char * argc[])
{
writestrm=fopen("allocation.txt","w");
	int st=atoi(argc[1]);
	int cr=atoi(argc[2]),i;
	pthread_t coalloc[1000000];
	sem_init(&gate,0,1);
	for( i=0;i<cr;i++)
		crgroup[i]=i%4;
	for(i=0;i<st;i++)
	{
			int j,cnt=0,ctaken[6]={0},taken[100]={0};
			for(j=0;j<cr;j++)
			{
					if(ctaken[crgroup[j]]==0)
					{
							ctaken[crgroup[j]]=1;
							pref[i][cnt++]=j;
							taken[j]=1;
					}
			}
			j=0;
			while(cnt!=8&&j<cr)
			{
					if(taken[j]==0)
					{
							pref[i][cnt++]=j;
					}
					j++;
			}
	}
	for(i=0;i<st/5;i++)
		strm[i]=1;
	for(;i<(2*st)/5;i++)
		strm[i]=2;
	for(;i<(4*st)/5;i++)
		strm[i]=3;
	for(;i<st;i++)
		strm[i]=4;
	for(i=0;i<st;i++)
	{
		stnu[i]=i;
		pthread_create(&coalloc[i],NULL,&calc,&stnu[i]);
	}
	for(i=0;i<st;i++)
		pthread_join(coalloc[i],NULL);

	return 0;
}
