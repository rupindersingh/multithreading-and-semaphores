#include<stdio.h>
#include<pthread.h>
#include<semaphore.h>
int h=0,o=0,s=0,e=0,curen=0,nu[200000];
sem_t wrlock,threshlock,site[2000000];
void *perform(void * arg)
{
		if(h<2||o<1)
		{
				return;
		}else{
				int siteno=*((int*)arg);
				sem_wait(&site[siteno]);
				sem_wait(&wrlock);
				if(curen==e-1)
				{
					sem_wait(&threshlock);
				}else if(curen==e)
				{
						sem_post(&wrlock);
						sem_wait(&threshlock);
						sem_wait(&wrlock);
						if(curen<e-1)
								sem_post(&threshlock);
				}
				if(h<2||o<1){sem_post(&wrlock);return;}
				int id=-1,i=0;
				/*while(1)
				{
						printf(".\n");
						int *temp;
						sem_getvalue(&sitenu[i+1], temp);
						if(*temp==0)
						{
								sem_wait(&sitenu[i+1]);
								id=i+1;
								break;
						}
						i=(i+1)%s;
				}*/
				printf("reaction startting: site[%d], total energy: %d, hydrogen left: %d, Oxygen left: %d\n",siteno,curen,h,o);
				curen+=1;
				h-=2;
				o-=1;
				printf("reaction in progress: site[%d], total energy: %d, hydrogen left: %d, Oxygen left: %d\n",siteno,curen,h,o);
				sem_post(&wrlock);
				sleep(3);
				sem_wait(&wrlock);
				curen-=1;
				printf("reaction finshed: site[%d], total energy: %d, hydrogen left: %d, Oxygen left: %d\n",siteno,curen,h,o);
				if(curen==e-1)
					sem_post(&threshlock);
				sem_post(&site[siteno]);
				sem_post(&wrlock);
		}
		return;
}
int main(int argv,char *argc[])
{
		int i;
		if(argv!=5)
		{
				printf("wrong number of arguments\n");
				return 0;
		}
		sem_init(&wrlock,0,1);
		sem_init(&threshlock,0,1);
		for( i=1;i<=2000000;i++)
				sem_init(&site[i],0,1);
		pthread_t moles[100000];
		h=atoi(argc[1]);
		o=atoi(argc[2]);
		s=atoi(argc[3]);
		e=atoi(argc[4]);
		int min=o<h/2?o:h/2,cnt=1;
		for(i=1;i<=min;i++)
		{
				nu[i]=cnt;
				pthread_create(&moles[i],NULL,perform,(void*)(&nu[i]));
				cnt+=2;
				if(cnt>s)
						cnt=1;
		}
		for(i=1;i<=min;i++)
		{
				pthread_join(moles[i],NULL);
		}
		return 0;
}
