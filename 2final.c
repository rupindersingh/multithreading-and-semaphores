#include<stdio.h>
#include<pthread.h>
#include<semaphore.h>
int g=0,ng=0,s=0,onb[1000000]={0},cur=-1,gw=0,ngw=0,sw=0;
int ge,nge,si;
sem_t wr[1000000],bridge,firstgate;
char names[3][100]={"geek","nongeek","singer"};
int cnt=0;
void init()
{
		int i;
		sem_init(&bridge,0,4);
		sem_init(&firstgate,0,1);
		for(i=0;i<1000000;i++)
		{
			sem_init(&wr[i],0,1);
		}
		return;
}

void go()
{
		int i;
		for( i=0;i<=cur;i++)
		{
				printf("%s ",names[onb[i]]);
				if(onb[i]==0)gw++;
				else if(onb[i]==1)ngw++;
				else sw++;
		}
		printf(" went across\n");
		ng=0;g=0;s=0;
		for(i=0;i<=cur;i++)
			sem_post(&bridge);
		cur=-1;
		return;
}

void* GeekArrives(void * arg)
{
		int i;
		sem_wait(&firstgate);
		while(ng==3||g==2&&ng==1)
		{
		//		printf("gl\n");
				if(ge-gw-g+nge-ngw-ng+(si-sw-s<=0?0:1)<4||(ng==3&&s==0&&si-sw-s==0))
				{sem_post(&firstgate);
						return;}
				sem_post(&firstgate);
				sem_wait(&firstgate);
		}
		sem_wait(&bridge);
		sem_getvalue(&bridge,&i);
		onb[++cur]=0;
		g++;
		if(i==0){
				go();
		}
		sem_post(&firstgate);
		return;
}

void *NonGeekArrives()
{
		int i;
		sem_wait(&firstgate);
		while(g==3||(ng==2&&g==1))
		{
		//		printf("ngl\n");
				if(ge-gw-g+nge-ngw-ng+(si-sw-s<=0?0:1)<4||(g==3&&si-sw-s==0&&s==0))
				{sem_post(&firstgate);
						return;}
				sem_post(&firstgate);
				sem_wait(&firstgate);
		}
		sem_wait(&bridge);
		sem_getvalue(&bridge,&i);
		onb[++cur]=1;
		ng++;
		if(i==0){
				go();
		}
		sem_post(&firstgate);
		return;
}

void *SingerArrives()
{
		int i;
		sem_wait(&firstgate);
		while(s==1)
		{
		//		printf("s\n");
				if(ge-gw-g+nge-ngw-ng<3)
				{sem_post(&firstgate);
						return;}
				sem_post(&firstgate);
				sem_wait(&firstgate);
		}
		sem_wait(&bridge);
		sem_getvalue(&bridge,&i);
		onb[++cur]=2;
		s++;
		if(i==0){
				go();
		}
		sem_post(&firstgate);
		return;
}

pthread_t entry[10000000];
int main(int argv,char * argc[])
{
		if(argv!=4)
		{
				printf("wrong number of arguments\n");
				return 0;
		}
		ge=atoi(argc[1]);
		nge=atoi(argc[2]);
		si=atoi(argc[3]);
		init();
		int i,j,k,l;
		for(l=0;l<si;l++)
				pthread_create(&entry[l],NULL,&SingerArrives,NULL);
		for(i=l;i<ge+l;i++)
				pthread_create(&entry[i],NULL,&GeekArrives,NULL);
		for(j=i;j<nge+i;j++)
				pthread_create(&entry[j],NULL,&NonGeekArrives,NULL);
		for(k=0;k<j;k++)
				pthread_join(entry[k],NULL);
		printf("%d geeks, %d nongeeks and %d singers couldnt go across\n",ge-g-gw,nge-ng-ngw,si-s-sw);
		return 0;
}
